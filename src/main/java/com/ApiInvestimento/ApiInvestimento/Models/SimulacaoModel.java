package com.ApiInvestimento.ApiInvestimento.Models;

import javax.persistence.*;

@Entity
public class SimulacaoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer mesesDeAplicacao;
    private Double dinheiroAplicado;

    @ManyToOne
    private InvestimentoModel investimento;

    public SimulacaoModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(Integer mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public Double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(Double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
   public InvestimentoModel getInvestimento() {
        return investimento;
    }

    public void setInvestimento(InvestimentoModel investimento) {
        this.investimento = investimento;
    }
}

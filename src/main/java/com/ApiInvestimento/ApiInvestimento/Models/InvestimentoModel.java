package com.ApiInvestimento.ApiInvestimento.Models;

import com.ApiInvestimento.ApiInvestimento.Enus.Risco;
import com.sun.istack.NotNull;
import org.aspectj.lang.annotation.RequiredTypes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class InvestimentoModel {
    public InvestimentoModel() {
    }

    public int getIdinvestimento() {
        return idinvestimento;
    }

    public void setIdinvestimento(int idinvestimento) {
        this.idinvestimento = idinvestimento;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idinvestimento;

    @NotEmpty
    @Size(min = 8, max = 100, message = "a descrição necessita conter entre 5 e 100 caracteres")
    private String descricao;

    private Risco risco;

    @DecimalMin("0.1")
    private double porcentagemlucro;


    @NotEmpty
    @Size(min = 8, max = 100, message = "a descrição necessita conter entre 5 e 100 caracteres")
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Risco getRisco() {
        return risco;
    }

    public void setRisco(Risco risco) {
        this.risco = risco;
    }

    public Double getPorcentagemlucro() {
        return porcentagemlucro;
    }

    public void setPorcentagemlucro(Double porcentagemlucro) {
        this.porcentagemlucro = porcentagemlucro;
    }



}

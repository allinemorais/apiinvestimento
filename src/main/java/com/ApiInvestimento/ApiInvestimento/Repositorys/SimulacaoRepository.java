package com.ApiInvestimento.ApiInvestimento.Repositorys;

import com.ApiInvestimento.ApiInvestimento.Models.InvestimentoModel;
import com.ApiInvestimento.ApiInvestimento.Models.SimulacaoModel;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<SimulacaoModel, Integer> {
}

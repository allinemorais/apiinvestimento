package com.ApiInvestimento.ApiInvestimento.Repositorys;

import com.ApiInvestimento.ApiInvestimento.Models.InvestimentoModel;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<InvestimentoModel, Integer> {
}

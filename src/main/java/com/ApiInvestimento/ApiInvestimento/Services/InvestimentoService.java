package com.ApiInvestimento.ApiInvestimento.Services;

import com.ApiInvestimento.ApiInvestimento.Models.InvestimentoModel;
import com.ApiInvestimento.ApiInvestimento.Repositorys.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService  {

    @Autowired
    InvestimentoRepository investimentoRepository;


    public InvestimentoModel SalvarInvestimento(InvestimentoModel investimentoPost)
    {
        InvestimentoModel investimentoModel = investimentoRepository.save(investimentoPost);
        return investimentoModel;
    }

    public  Optional<InvestimentoModel>  buscarPorId(Integer id){
        Optional<InvestimentoModel> investimentoOptional = investimentoRepository.findById(id);
        return  investimentoOptional;

    }

    public Iterable<InvestimentoModel>  buscarTodos()
    {
        Iterable<InvestimentoModel>  investimentoModel = investimentoRepository.findAll();
        return investimentoModel;
    }

    public InvestimentoModel atualizarIvenstimento(InvestimentoModel investimento)
    {
        Optional<InvestimentoModel> investimentoptional = investimentoRepository.findById(investimento.getIdinvestimento());
        InvestimentoModel investimentoData = investimentoptional.get();

        if(investimentoptional.isPresent())
        {
            if(investimento.getNome() == null)
            {
                investimento.setNome(investimentoData.getNome());
            }

            if(investimento.getDescricao() == null)
            {
                investimento.setNome(investimentoData.getDescricao());
            }

            if(investimento.getRisco() == null)
            {
                investimento.setRisco(investimentoData.getRisco());
            }

        }

        investimentoData = investimentoRepository.save(investimento);
        return investimento;
    }

    public InvestimentoModel deletarInvestimento(InvestimentoModel investimento)
    {
        investimentoRepository.delete(investimento);
        return investimento;
    }


}

package com.ApiInvestimento.ApiInvestimento.Services;

import com.ApiInvestimento.ApiInvestimento.Models.InvestimentoModel;
import com.ApiInvestimento.ApiInvestimento.Models.SimulacaoModel;
import com.ApiInvestimento.ApiInvestimento.Repositorys.InvestimentoRepository;
import com.ApiInvestimento.ApiInvestimento.Repositorys.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimularService {


    @Autowired
    SimulacaoRepository simularRepository;


    public SimulacaoModel SalvarSimulacao(SimulacaoModel simulacao)
    {
        SimulacaoModel simulacaoModel = simularRepository.save(simulacao);
        return simulacaoModel;
    }

    public Iterable<SimulacaoModel>  buscarTodos()
    {
        Iterable<SimulacaoModel>  simulacaoModel = simularRepository.findAll();
        return simulacaoModel;
    }

}

package com.ApiInvestimento.ApiInvestimento.Controller;
import com.ApiInvestimento.ApiInvestimento.Models.InvestimentoModel;
import com.ApiInvestimento.ApiInvestimento.Services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/Investimento")
public class InvestimentoController {

    public InvestimentoController() {
    }

    @Autowired
    private InvestimentoService investimentoService;


    @PostMapping
    public InvestimentoModel salvarInvestimento(@RequestBody InvestimentoModel investimentoPost)
    {
        InvestimentoModel investimentoModel = investimentoService.SalvarInvestimento(investimentoPost);
        return  investimentoModel;
    }

    @GetMapping
    public Iterable<InvestimentoModel> buscarTodosInvestimentos(){
        return investimentoService.buscarTodos();
    }

    @GetMapping("/{id}")
    public InvestimentoModel buscarInvestimento(@PathVariable Integer id){
        Optional<InvestimentoModel> investimentoOptional = investimentoService.buscarPorId(id);

        if(investimentoOptional.isPresent()){
            return investimentoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PutMapping("/{id}")
    public InvestimentoModel atualizarInvestimento(@PathVariable Integer id, @RequestBody InvestimentoModel investimento)
    {
        investimento.setIdinvestimento(id);
        InvestimentoModel leadobjeto = investimentoService.atualizarIvenstimento(investimento);
        return investimento;
    }

    @DeleteMapping("/{id}")
    public Optional<InvestimentoModel>  deletarInvestimento(@PathVariable Integer id)
    {
        Optional<InvestimentoModel> investimentodOPT = investimentoService.buscarPorId(id);
        if(investimentodOPT.isPresent())
        {
            investimentoService.deletarInvestimento(investimentodOPT.get());
        }
        return investimentodOPT;
    }




}

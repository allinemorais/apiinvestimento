package com.ApiInvestimento.ApiInvestimento.Controller;

import com.ApiInvestimento.ApiInvestimento.Models.InvestimentoModel;
import com.ApiInvestimento.ApiInvestimento.Models.SimulacaoModel;
import com.ApiInvestimento.ApiInvestimento.Services.InvestimentoService;
import com.ApiInvestimento.ApiInvestimento.Services.SimularService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {
    public SimulacaoController() {


    }

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimularService simulacaoService;


    @PostMapping
    public SimulacaoModel salvarSimulacao(@RequestBody SimulacaoModel simulacaoModel)
    {
        SimulacaoModel simulacaoobj = simulacaoService.SalvarSimulacao(simulacaoModel);
        Optional<InvestimentoModel> investimentoOpt= investimentoService.buscarPorId(simulacaoModel.getId());

        if(investimentoOpt.isPresent()) {
            simulacaoobj.setDinheiroAplicado((simulacaoobj.getMesesDeAplicacao() * simulacaoobj.getDinheiroAplicado()) * investimentoOpt.get().getPorcentagemlucro());
            return simulacaoobj;
        }

        return  simulacaoobj;
    }

    @GetMapping
    public Iterable<SimulacaoModel> buscarTodosInvestimentos(){
        return simulacaoService.buscarTodos();
    }

}
